package pt.ipp.isep.restdemo.DTO;

import org.springframework.hateoas.ResourceSupport;
import pt.ipp.isep.restdemo.model.Product;


@lombok.Getter
public class ProductDTO extends ResourceSupport {

    //private final Product product;

    private final Long productId;

    private final String name;

    public ProductDTO(final Product product) {
        //this.product = product;
        productId = product.getId();
        name = product.getName();
    }
}
