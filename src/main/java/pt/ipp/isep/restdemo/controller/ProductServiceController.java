package pt.ipp.isep.restdemo.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.util.*;

import pt.ipp.isep.restdemo.DTO.ProductDTO;
import pt.ipp.isep.restdemo.exception.ProductNotFoundException;
import pt.ipp.isep.restdemo.model.*;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@Scope("request")
@CrossOrigin
public class ProductServiceController {
    private Map<Long, Product> productRepo = new HashMap<>();

    {
        Product bolachas = new Product();
        bolachas.setId(1L);
        bolachas.setName("Bolachas");
        productRepo.put(bolachas.getId(), bolachas);

        Product cafe = new Product();
        cafe.setId(2L);
        cafe.setName("Café");
        productRepo.put(cafe.getId(), cafe);
    }

    private final ProductRepository repository;

    ProductServiceController(ProductRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/productsHashMap/{id}", produces = { "application/hal+json" })
    public ResponseEntity<Product> getProduct(@PathVariable("id") Long id) {

        Optional<Product> product = repository.findById(id);
        if (!product.isPresent())
            throw new ProductNotFoundException("id-" + id);

        return new ResponseEntity<>(product.get(), HttpStatus.OK);
    }


    @RequestMapping(value = "/productsHashMap/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        if(productRepo.remove(id) != null)
            return new ResponseEntity<>("Product is deleted successsfully", HttpStatus.OK);
        else
            return new ResponseEntity<>("Product is not found???", HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/productsHashMap/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateProduct(@PathVariable("id") Long id, @RequestBody Product product) {
        productRepo.remove(id);
        product.setId(id);
        productRepo.put(id, product);
        return new ResponseEntity<>("Product is updated successsfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/productsHashMap", method = RequestMethod.POST)
    public ResponseEntity<Object> createProduct(@RequestBody Product product) {
        productRepo.put(product.getId(), product);
        return new ResponseEntity<>("Product is created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/productsHashMap")
    public ResponseEntity<Object> getProducts() {

        return new ResponseEntity<>(productRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "/productsDB/{id}", produces = { "application/hal+json" })
    public ResponseEntity<Resource> getProductDBById(@PathVariable("id") Long id) {

        Optional<Product> product = repository.findById(id);
        if (!product.isPresent())
            throw new ProductNotFoundException("id-" + id);
        Resource<Product> resource = new Resource<Product>(product.get());
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).updateProduct(id, null));
        resource.add(linkTo.withSelfRel());

        HttpHeaders headers = getAllowHeaders(product.get());

        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/productsDB/{id}", method = RequestMethod.OPTIONS, produces = { "application/hal+json" })
    public ResponseEntity<Resource> getOptions(@PathVariable("id") Long id) {

        Optional<Product> product = repository.findById(id);
        if (!product.isPresent())
            throw new ProductNotFoundException("id-" + id);

        return new ResponseEntity<>(getAllowHeaders(product.get()), HttpStatus.NO_CONTENT);
    }

    private HttpHeaders getAllowHeaders(Product product) {

        // "allow" headers depends on authorization (not defined yet)
        // HttpHeaders headers = determineAuthorizations(product);

        HttpHeaders headers = new HttpHeaders();
        Set<HttpMethod> allow = new HashSet<>();
        allow.add(HttpMethod.POST);
        allow.add(HttpMethod.PUT);
        allow.add(HttpMethod.DELETE);
        allow.add(HttpMethod.OPTIONS);
        allow.add(HttpMethod.GET);
        headers.setAllow(allow);

        return headers;
    }

    @RequestMapping(value = "/productsDB")
    public ResponseEntity<Object> getProductDB() {
        return new ResponseEntity<>(repository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/productsDBLinks", produces = { "application/hal+json" })
    public ResponseEntity<Object> getProductsDBLinks() {
        Iterable<Product> products = repository.findAll();

        Set<ProductDTO> productsDTO = new HashSet<>();

        for (final Product product : products) {
            Link selfLink = linkTo(methodOn(ProductServiceController.class)
                    .getProductDBById(product.getId())).withSelfRel();

            ProductDTO productDTO = new ProductDTO(product);
            productDTO.add(selfLink);

            ((HashSet<ProductDTO>) productsDTO).add(productDTO);
        }

        Link link = linkTo(methodOn(ProductServiceController.class)
                .getProductDB()).withSelfRel();

        Resources<ProductDTO> result = new Resources<ProductDTO>(productsDTO, link);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}