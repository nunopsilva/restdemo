package pt.ipp.isep.restdemo.model;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DbInit implements CommandLineRunner {
    private UserRepository userRepository;
    private ProductRepository productRepository;
    private PasswordEncoder passwordEncoder;

    public DbInit(UserRepository userRepository, ProductRepository productRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        // Delete all
        this.userRepository.deleteAll();

        // Crete users
        User owner = new User("owner",passwordEncoder.encode("owner123"),"OWNER","");
        User admin = new User("admin",passwordEncoder.encode("admin123"),"ADMIN","ACCESS_TEST1,ACCESS_TEST2");
        User manager = new User("manager",passwordEncoder.encode("manager123"),"MANAGER","ACCESS_TEST1");

        List<User> users = Arrays.asList(owner,admin,manager);

        // Save to db
        this.userRepository.saveAll(users);





        // Delete all
        this.productRepository.deleteAll();

		// save a couple of products
		this.productRepository.save(new Product(1L, "Canela"));
		this.productRepository.save(new Product(2L, "Açafrão"));
		this.productRepository.save(new Product(3L, "Nóz-moscada"));
		this.productRepository.save(new Product(4L, "Pimenta"));
		this.productRepository.save(new Product(5L, "Cravinho"));

	}
}