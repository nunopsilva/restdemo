package pt.ipp.isep.restdemo.application.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import pt.ipp.isep.restdemo.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductServiceIntegrationTest {

    private static final String PRODUCTS_PATH = "/productsHashMap/";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenReadAll_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(PRODUCTS_PATH))
                .andExpect(status().isOk());
    }

    @Test
    public void whenReadOne_thenStatusIsOk() throws Exception {
        this.mockMvc.perform(get(PRODUCTS_PATH + 1))
                .andExpect(status().isOk());
    }

    @Test
    public void whenCreate_thenStatusIsCreated() throws Exception {
        Product product = new Product(10L, "laranjada");
        this.mockMvc.perform(post(PRODUCTS_PATH).content(asJsonString(product))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void whenUpdate_thenStatusIsOk() throws Exception {
        Product product = new Product(1L, "Bolachas de Chocolate");
        this.mockMvc.perform(put(PRODUCTS_PATH + 1)
                .content(asJsonString(product))
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDelete_thenStatusIsNotFound() throws Exception {
        this.mockMvc.perform(delete(PRODUCTS_PATH + 36))
                .andExpect(status().isNotFound());
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}